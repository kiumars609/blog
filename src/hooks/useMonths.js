import React, { useState, useEffect } from 'react'

const useMonths = (value) => {
    const [resultValue, setResultValue] = useState(value);

    useEffect(() => {
        if (value === '01') {
            setResultValue('January')
        }
        if (value === '02') {
            setResultValue('February')
        }
        if (value === '03') {
            setResultValue('March')
        }
        if (value === '04') {
            setResultValue('April')
        }
        if (value === '05') {
            setResultValue('May')
        }
        if (value === '06') {
            setResultValue('June')
        }
        if (value === '07') {
            setResultValue('July')
        }
        if (value === '08') {
            setResultValue('August')
        }
        if (value === '09') {
            setResultValue('September')
        }
        if (value === '10') {
            setResultValue('October')
        }
        if (value === '11') {
            setResultValue('November')
        }
        if (value === '12') {
            setResultValue('December')
        }
    }, []);

    return resultValue
}

export default useMonths;