import React from 'react'

const usePagination = ({ postsPerPage, totalPosts, paginate }) => {
    const pageNumbers = [];
    
    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <nav>
            <ul className='pagination'>
                {pageNumbers.map(number => (
                    <li key={number} className="page-item">
                        <a onClick={() => paginate(number)} className='page-link' role="button">
                            {number}
                        </a>
                    </li>
                ))}
            </ul>
        </nav>
    )
}

export default usePagination