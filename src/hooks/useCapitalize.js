import React, { useState, useEffect } from 'react'

const useCapitalize = (value) => {
    return value.charAt(0).toUpperCase() + value.slice(1);
}

export default useCapitalize;