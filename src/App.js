import './App.css';
import "./i18n";
import Blog from './Pages/Blog';
import PostDetail from './Pages/Blog/PostDetail';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Category from './Pages/Category/Category';
import GameDetail from './Pages/GameDetail/GameDetail';
import Login from './Pages/Login/Login';
// import { useTranslation } from 'react-i18next';



function App() {
  return (
    <>
      {/* <nav style={{ width: '100%', padding: '2rem 0', backgroundColor: 'gray' }}>
        <button onClick={() => handleClick('en')}>
          En
        </button>
        <button onClick={() => handleClick('du')}>
          Du
        </button>
      </nav> */}
      <Router>
        <Switch>
          <Route exact path="/game-deutch">
            <Blog />
          </Route>
          <Route path="/game-deutch/detail/:id">
            <PostDetail />
          </Route>
          <Route path="/game-deutch/category/:name">
            <Category />
          </Route>
          <Route path="/game-deutch/game-detail/:id">
            <GameDetail />
          </Route>
          <Route path="/game-deutch/login">
            <Login />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
