import React from 'react'
import Search from '../Search/Search'
import RecentPost from './RecentPost'
import Social from './Social'
import TopGames from './TopGames'

export default function RightSide() {
    return (
        <>
            <div className="col-lg-3">
                <Search />
                <TopGames />
                <RecentPost />
                <Social />
            </div>
        </>
    )
}
