import React, { useEffect, useState } from 'react'
import { get } from '../../services/HttpClient'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next';

export default function TopGames() {
    const { t, i18n } = useTranslation();
    const [data, setData] = useState([])
    useEffect(() => {
        get('/topGames/topGamesAPI.php')
            .then(response => {
                setData(response);
            })
            
    }, []);

    const localCapitalize = (val) => {
        return val.charAt(0).toUpperCase() + val.slice(1)
    }

    const mapData = data && data.map((row, index) => {
        return (
            <li key={index}>
                <Link to={`/game-deutch/game-detail/${row.id}`}>{localCapitalize(row.title)}</Link>
            </li>
        )
    })

    return (
        <div className="section-container bg-white rounded p-5 shadow">
            <h4 className="section-title"><span>{t('TOPGAMES.1')}</span></h4>
            <ul className="sidebar-list">
                {mapData}
            </ul>
        </div>
    )
}
