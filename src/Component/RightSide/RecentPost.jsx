import React, { useEffect, useState } from 'react'
import { get } from '../../services/HttpClient';
import Loading from '../Loading/Loading'
import './style.css'
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

export default function RecentPost() {
    const { t, i18n } = useTranslation();
    const [data, setData] = useState([])
    const [loader, setLoader] = useState(true)
    useEffect(() => {
        get('/recentPosts/postsAPI.php')
            .then(response => {
                setData(response);
                setLoader(false)
            })
    }, []);

    const mapData = data && data.map((item, index) => {
        const fetchCreateAt = item.createdAt.slice(0, 10).replace(/-/g, '/');
        return (
            <li key={index}>
                <div className="info">
                    <h4 className="title">
                        <Link to={`/game-deutch/detail/id=${item.id}`}>{item.title}</Link>
                    </h4>
                    <div className="date">{fetchCreateAt}</div>
                </div>
            </li>
        )
    })

    return (
        <>
            <div className="section-container bg-white rounded p-5 shadow">
                <h4 className="section-title"><span>{t('Recent-Post.1')}</span></h4>
                <ul className="sidebar-recent-post">
                    {loader ? <Loading /> : mapData}
                </ul>
            </div>
        </>
    )
}
