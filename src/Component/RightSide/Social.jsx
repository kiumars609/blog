import React from 'react'
import { useTranslation } from 'react-i18next';

export default function Social() {
    const { t, i18n } = useTranslation();
    return (
        <>
            <div className="section-container bg-white rounded p-5 shadow">
                <h4 className="section-title"><span>{t('Follow-Us.1')}</span></h4>
                <ul className="sidebar-social-list">
                    <li><a href="https://www.instagram.com/about.kiumars/" target={'_blank'}><i className="fab fa-instagram"></i></a></li>
                    <li><a href="https://t.me/Kiumars_Gamer" target={'_blank'}><i className="fab fa-telegram"></i></a></li>
                </ul>
            </div>
        </>
    )
}
