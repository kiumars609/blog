import React, { useEffect, useState } from 'react'
import useDebounce from '../../hooks/useDebounce';
import { Link } from 'react-router-dom';
import { get } from '../../services/HttpClient';
import './style.css'
import { useTranslation } from 'react-i18next';

export default function Search() {
    const { t, i18n } = useTranslation();
    const [data, setData] = useState([])
    const [search, setSearch] = useState('')
    const [filterSearch, setFilterSearch] = useState([])
    const debouncedSearchTerm = useDebounce(search, 1500)
    useEffect(() => {
        get('/posts/postsAPI.php')
            .then(response => {
                setData(response)
            })
    }, []);

    useEffect(() => {
        setFilterSearch(
            data.filter(results => {
                return results.title.toLowerCase().includes(debouncedSearchTerm.toLowerCase())
            })
        )
        if (search !== '') {
            document.getElementById('mains').className = "col-12 main-results-search d-block"
        }
        else {
            document.getElementById('mains').className = "d-none"
        }
    }, [debouncedSearchTerm, data, search]);

    const mapData = filterSearch.map((row, index) => {
        return (
            <Link to={`/game-deutch/detail/id=${row.id}`} key={index} className='search-results'>{row.title}</Link>
        )
    })

    const closeSearch = () => {
        document.getElementById('mains').className = "d-none"
    }

    return (
        <div className="section-container">
            <div className="input-group sidebar-search">
                <input type="text" className="form-control" placeholder={t('Search-Our-Posts.1')} onChange={e => setSearch(e.target.value)} />
                <span className="input-group-append">
                    <button className="btn btn-inverse" type="button"><i className="fa fa-search"></i></button>
                </span>
            </div>
            <div className='col-12 main-results-search' id='mains'>
                <i className='fa fa-close mt-2 text-danger' role="button" onClick={closeSearch}></i>
                {mapData}
            </div>
        </div>
    )
}