import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom';
import { get } from '../../services/HttpClient';
import { useTranslation } from 'react-i18next';

export default function HotNews() {
    const { t, i18n } = useTranslation();
    const [data, setData] = useState([])
    useEffect(() => {
        get('/posts/postsAPI.php')
            .then(response => setData(response))
    }, []);

    const mapData = data && data.map((item, index) => {
        return (
            <li key={index}>
                <Link key={index} to={`/game-deutch/detail/id=${item.id}`}>{item.title}</Link>
            </li>
        )
    })

    return (
        <>
            <div className="col-12 bg-white rounded shadow my-3">
                <h5 className='fs-6 py-2 title-hot-news'>
                    {t('Hot-News.1')}
                </h5>
                <ul className='main-hot-news-links pb-3'>
                    {mapData}
                </ul>
            </div>
        </>
    )
}
