import React from 'react'
import { Link } from 'react-router-dom'
import ModalLanguage from './ModalLanguage'
import { useTranslation } from 'react-i18next';
import './style.css'

export default function Header() {
    const { t, i18n } = useTranslation();
    return (
        <>
            <div id="header" className="header navbar navbar-transparent navbar-expand-lg navbar-fixed-top">
                <div className="container">
                    <span className="navbar-brand">
                        <span className="brand-logo"></span>
                        <span className="brand-text">
                            <Link to={'/game-deutch'}>GameDeutch</Link>
                        </span>
                        <span className="brand-text ml-3">
                            <i className="fa fa-language fa-1x text-primary select-lang" aria-hidden="true" data-bs-toggle="modal" data-bs-target="#exampleModal"></i>
                        </span>
                        <span className="brand-text ml-3">
                            <Link to={'/game-deutch/login'}><i className="fa fa-user text-primary select-lang"></i></Link>
                        </span>
                    </span>
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="header-navbar">
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <Link to={'/game-deutch'}>{t('Menu-Home.1')}</Link>
                            </li>
                            <li className="dropdown">
                                <a href='/#' data-toggle="dropdown">{t('Menu-Category.1')} <b className="caret"></b></a>
                                <div className="dropdown-menu">
                                    <Link to={'/game-deutch/category/pc'} className='dropdown-item'>PC</Link>
                                    <Link to={'/game-deutch/category/ps4'} className='dropdown-item'>PS4</Link>
                                    <Link to={'/game-deutch/category/ps5'} className='dropdown-item'>PS5</Link>
                                    <Link to={'/game-deutch/category/xbox'} className='dropdown-item'>XBOX</Link>
                                    <Link to={'/game-deutch/category/movie'} className='dropdown-item'>{t('Movie.1')}</Link>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ModalLanguage />
        </>
    )
}
