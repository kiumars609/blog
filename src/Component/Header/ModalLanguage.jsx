import React from 'react'
import { useTranslation } from 'react-i18next';

export default function ModalLanguage() {
    const { t, i18n } = useTranslation();
    function handleClick(lang) {
        i18n.changeLanguage(lang);
    }

    return (
        <>
            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">{t('Select-Your-Language.1')}</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body d-flex justify-content-center">
                            <span onClick={() => handleClick('de')} className='mx-2' role="button"><img src={'./assets/img/icon/du-flag.png'} alt='' width={30} /></span>
                            <span onClick={() => handleClick('en')} className='mx-2' role="button"><img src={'./assets/img/icon/en-flag.png'} alt='' width={30} /></span>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
