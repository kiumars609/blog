import React from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next';

export default function Footer() {
    const { t, i18n } = useTranslation();
    return (
        <>
            <div id="footer" className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3 col-6">
                            <div className="section-container">
                                <h4 className="footer-title">Categories</h4>
                                <ul className="categories">
                                    <li><Link to={'/game-deutch/category/pc'}>PC</Link></li>
                                    <li><Link to={'/game-deutch/category/ps4'}>PS4</Link></li>
                                    <li><Link to={'/game-deutch/category/ps5'}>PS5</Link></li>
                                    <li><Link to={'/game-deutch/category/xbox'}>XBOX</Link></li>
                                    <li><Link to={'/game-deutch/category/movie'}>{t('Movie.1')}</Link></li>
                                </ul>
                            </div>
                        </div>
                        {/* <div className="col-md-3 col-6">
                            <div className="section-container">
                                <h4 className="footer-title">Archives</h4>
                                <ul className="archives">
                                    <li><a href="#">June 2018</a> <span className="total">(102)</span></li>
                                    <li><a href="#">May 2018</a> <span className="total">(46)</span></li>
                                    <li><a href="#">April 2018</a> <span className="total">(84)</span></li>
                                    <li><a href="#">March 2018</a> <span className="total">(67)</span></li>
                                    <li><a href="#">February 2018</a> <span className="total">(99)</span></li>
                                    <li><a href="#">January 2018</a> <span className="total">(113)</span></li>
                                    <li><a href="#">December 2017</a> <span className="total">(25)</span></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-3 col-6">
                            <div className="section-container">
                                <h4 className="footer-title">Recent Posts</h4>
                                <ul className="recent-post">
                                    <li>
                                        <h4>
                                            <a href="#">Nam ut urna hendrerit</a>
                                            <span className="time">February 22, 2018</span>
                                        </h4>
                                    </li>
                                    <li>
                                        <h4>
                                            <a href="#">Class aptent taciti sociosqu</a>
                                            <span className="time">July 15, 2018</span>
                                        </h4>
                                    </li>
                                    <li>
                                        <h4>
                                            <a href="#">Donec rhoncus arcu</a>
                                            <span className="time">March 21, 2018</span>
                                        </h4>
                                    </li>
                                </ul>
                            </div>
                        </div> */}
                        <div className="col-md-3 col-6">
                            <div className="section-container">
                                <h4 className="footer-title">About Creator</h4>
                                <address>
                                    <strong>Full Name</strong><br />
                                    <span>Kiumars Sezavar Kamali</span>
                                    <br />
                                    <strong>Telephon</strong><br />
                                    <span>+98 915 504 2678</span>
                                    <br />
                                    <strong>Email</strong><br />
                                    <span>kiumars.dev@gmail.com</span>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="footer-copyright" className="footer-copyright">
                <div className="container">
                    <span className="copyright">&copy; 2022 609 All Right Reserved</span>
                    <ul className="social-media-list mt-2 mt-sm-0 float-none float-sm-right">
                        <li><a href="https://www.instagram.com/about.kiumars/" target='_blank' rel='noreferrer'><i className="fab fa-instagram"></i></a></li>
                        <li><a href="https://t.me/Kiumars_Gamer" target='_blank' rel='noreferrer'><i className="fab fa-telegram"></i></a></li>
                    </ul>
                </div>
            </div>
        </>
    )
}
