import React from 'react'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import Post from '../Post/Post'
import RightSide from '../RightSide/RightSide'
import './style.css'

export default function Layout({ children, hotNews = false }) {
    return (
        <>
            <div className='blog-site'>
                <Header />
                <div id="page-title" className="page-title has-bg">
                    {/* style="background: url() center 0px / cover no-repeat" */}
                    <div className="bg-cover" data-paroller="true" data-paroller-factor="0.5" data-paroller-factor-xs="0.2" style={{ backgroundImage: "url('https://wallpaperaccess.com/full/7414713.jpg')" }}></div>
                    <div className="container">
                        {/* <h1>Official 609 Blog</h1> */}
                        {/* <p>Blog Concept Front End Page</p> */}
                    </div>
                </div>
                {/* First Ad */}
                <div className="col-md-10 col-sm-12 rounded my-3 row mx-auto bg-white main-first-ad">
                    <img src='https://gamefa.com/wp-content/uploads/2022/04/gamefabanner.jpg.webp' className='col-md-12 col-sm-12 my-2' alt='' />
                    <img src='https://gamefa.com/wp-content/uploads/2021/12/ezgif-6-ac9bfc24f2ba.gif' className='col-md-6 col-sm-12 mb-2' alt='' />
                    <img src='https://gamefa.com/wp-content/uploads/2021/12/60-600-FINAL-1.gif' className='col-md-6 col-sm-12 mb-2' alt='' />
                    <img src='https://gamefa.com/wp-content/uploads/2022/02/sceptre.gif' className='col-md-6 col-sm-12 mb-2' alt='' />
                    <img src='https://gamefa.com/wp-content/uploads/2021/12/ezgif-6-da4471bb0edf.gif' className='col-md-6 col-sm-12 mb-2' alt='' />
                </div>
                {/* Hot News */}
                {hotNews && <div className="col-md-10 col-sm-12 rounded my-3 row mx-auto bg-white">
                    <label className='col-md-6 col-sm-12 my-2 position-relative label-image-ad'>
                        <img src='https://gamefa.com/wp-content/uploads/2022/04/IMG_20220421_205533_472.jpg.webp' className='img-thumbnail' alt='' />
                        <span>Batman</span>
                    </label>
                    <label className='col-md-6 col-sm-12 my-2 position-relative label-image-ad'>
                        <img src='https://gamefa.com/wp-content/uploads/2022/04/god-of-war-ragnarok-ps5-playstation-5-1.jpg.webp' className='img-thumbnail' alt='' />
                        <span>God Of War</span>
                    </label>
                </div>}




                <div id="content" className="content">
                    <div className="container">
                        <div className="row row-space-30">
                            <Post>
                                {children}
                            </Post>
                            <RightSide />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </>
    )
}