import React from 'react'

export default function Post({ children }) {
    return (
        <div className="col-lg-9">
            <ul className="post-list">
                {children}
            </ul>
        </div>
    )
}
