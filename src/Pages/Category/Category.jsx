import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Layout from '../../Component/Layout'
import Loading from '../../Component/Loading/Loading';
import { get } from '../../services/HttpClient';
import SinglePost from '../Blog/SinglePost';

export default function Category() {
    const fetchName = useParams()
    const [data, setData] = useState([] || null)
    const [loader, setLoader] = useState(true)

    useEffect(() => {
        get(`/posts/postsAPI.php?category=${fetchName.name}`)
            .then(response => {
                setData(response);
                if (response !== undefined) {
                    setLoader(false)
                }
            })
    }, [fetchName]);

    const mapData = data && data.map((row, index) => {
        if (row.categoryTitle === fetchName.name) {
            return (
                <SinglePost
                    key={index}
                    id={row.id}
                    title={row.title}
                    text={row.text}
                    datePost={row.createdAt}
                    image={row.image}
                    categoryTitle={row.categoryTitle}
                />
            )
        }
    })

    return (
        <>
            <Layout>
                {loader ? <Loading /> : mapData}
            </Layout>
        </>
    )
}
