import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

export default function Login() {
    return (
        <>
            <input type="checkbox" id="change" hidden />
            <Link to={'./'} className='back-house-link'><i className='fa fa-house'></i></Link>
            <section className="main-login-panel row mx-auto p-0 col-md-12">
                <div className="col-md-6 left px-0">
                    <div className="login-panel col-md-6">
                        <form>
                            <label className='col-md-12'>
                                <h5>HeroName</h5>
                                <input type='text' className='form-control' />
                            </label>
                            <label className='col-md-12'>
                                <h5>HeroPass</h5>
                                <input type='text' className='form-control' />
                            </label>
                            <span className='col-md-12 my-1 d-block'>
                                <button className='btn btn-login' disabled>LogIn</button>
                            </span>
                        </form>
                        <div className="col-md-12">
                            Don't have an account? <label htmlFor="change" className='change-panel'>Sign up</label>
                        </div>
                    </div>


                    <div className="register-panel col-md-6">
                        <form>
                            <label className='col-md-12'>
                                <h5>HeroName</h5>
                                <input type='text' className='form-control' />
                            </label>
                            <label className='col-md-12'>
                                <h5>HeroPass</h5>
                                <input type='text' className='form-control' />
                            </label>
                            <label className='col-md-12'>
                                <h5>HeroMail</h5>
                                <input type='text' className='form-control' />
                            </label>
                            <span className='col-md-12 my-1 d-block'>
                                <button className='btn btn-register' disabled>Register</button>
                            </span>
                        </form>
                        <div className="col-md-12">
                            Have an account? <label htmlFor="change" className='change-panel'>Sign in</label>
                        </div>
                    </div>


                </div>
                <div className="col-md-6 right px-0" id="sony">
                    <img src={'./assets/img/back/login/sonyLogo.png'} alt='' />
                </div>
                <div className="col-md-6 right px-0" id="xbox">
                    <img src={'./assets/img/back/login/xboxLogo.jpg'} alt='' />
                </div>
            </section>
        </>
    )
}
