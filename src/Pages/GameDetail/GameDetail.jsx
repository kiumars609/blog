import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Layout from '../../Component/Layout'
import { get } from '../../services/HttpClient';
import Details from './Details';
import Slider from './Slider';
import './style.css'
import Trailer from './Trailer';
import Loading from '../../Component/Loading'

export default function GameDetail() {
    const fetchId = useParams();
    const [data, setData] = useState([])
    const [loader, setLoader] = useState(true);
    useEffect(() => {
        get(`/topGames/topGamesAPI.php?id=${fetchId.id}`)
            .then(response => {
                for (let index = 0; index < response.length; index++) {
                    if (response[index].id === fetchId.id) {
                        const element = response[index];
                        setData(element)
                        setLoader(false)
                    }
                }
            })
    }, [fetchId]);

    return (
        <>
            {/* Game Detail */}
            <Layout>
                {/* All Details */}
                {loader ? <Loading /> :
                    <>
                        <Details data={data} />
                        <Slider data={data} />
                        <Trailer data={data} />
                    </>
                }
            </Layout>
        </>
    )
}