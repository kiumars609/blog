import { t } from 'i18next'
import React from 'react'
import useCapitalize from '../../hooks/useCapitalize'
import { useTranslation } from 'react-i18next';

export default function Details({ data }) {
    const { t, i18n } = useTranslation();
    const splitPlatform = data.platform.split(',')

    const mapPlatform = splitPlatform.map((item, index) => {
        return (
            <span className="m-2" key={index}>
                <a href className="text-white fnt-light">
                    {item.toUpperCase()}
                </a>
            </span>
        )
    })

    
    return (
        <>
            <div className="col-md-12 col-sm-12 main-detail rounded shadow">
                <div className="row mx-auto header">
                    <div className="col-md-1 col-sm-1">
                        <span className="fas fa-bookmark pr-1"><i className="fas fa-plus text-white"></i></span>
                    </div>
                    <div className="col-md-11 col-sm-2 title pl-0">
                        {useCapitalize(data.title)}
                    </div>
                </div>
                <div className="row mx-auto main">
                    {/* Image Game (Left Side) */}
                    <div className="col-3 p-0 main-img">
                        <img src={data.img} className='col-12 rounded' alt='' />
                    </div>
                    {/* Details Game (Right Side) */}
                    <div className="col-9">
                        <div className="col-12 main-rate p-0">
                            {/* Main Rates Top Container */}
                            <div className="row mx-auto col-8">
                                <div className="pb-3 clearfix d-flex justify-content-between">
                                    <div className="float-left d-flex">
                                        <div className="pt-1">
                                            <i className="fas fa-star text-warning pt-2"></i>
                                        </div>
                                        <div className="position-relative">
                                            <span className="score">
                                                <small>{data.siteRate}/10</small>
                                            </span>
                                            <span className="text">{t('GameDeutch-Rate.1')}</span>
                                        </div>
                                    </div>
                                    <div className="float-left text-left user small-score ml-5">
                                        <div className="position-relative">
                                            <span className="score">
                                                <small>{data.userRate}/10</small>
                                            </span>
                                            <span className="text">{t('Users-Rate.1')}</span>
                                        </div>
                                    </div>
                                    <div className="float-left pl-2 small-score"></div>
                                </div>
                            </div>
                            {/* Main Rates Down Container */}
                            <div className="row mx-auto col-12">
                                <div id="ASRating_games-container" className='col-12 mx-auto mt-1'>
                                    <div id="ASRating_games" className="clearfix row mx-auto">
                                        <div className="stars col-6 text-right">
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star star-gold"></span>
                                            <span className="star"></span>
                                        </div>
                                        <div className="score col-6 text-left">9.2 {t('Based-On.1')} 2,250 {t('User-Ratings.1')}</div>
                                    </div>
                                    <div id="ASRating_games-waiting">
                                        <img src="https://gamefa.com/wp-content/plugins/ASRating-games/public/images/spinner.gif" alt="spinner" />
                                        <span>در حال ثبت رای</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 row mx-auto main-detail">
                            {/* Complete Details (Left Side) */}
                            <div className="col-md-6 col-sm-12 pt-4 main-table">
                                <table className="w-100 text-white font-size-1 mr-auto fnt-light">
                                    <tbody>
                                        <tr className="py-2">
                                            <td className="clr1 px-2 pl-4 py-2">
                                                {t('Release-Date.1')}
                                            </td>
                                            <td className="clr2 px-2 py-2">
                                                {data.releaseDate}
                                            </td>
                                        </tr>
                                        <tr className="py-2 px-1">
                                            <td className="clr2 px-2 pl-4 py-2">
                                                {t('Genre.1')}
                                            </td>
                                            <td className="clr3 px-2 py-2">
                                                {useCapitalize(data.genre)}
                                            </td>
                                        </tr>
                                        <tr className="py-2 px-1">
                                            <td className="clr1 px-2 pl-4 py-2">
                                                {t('Developer.1')}
                                            </td>
                                            <td className="clr2 px-2 py-2">
                                                {useCapitalize(data.developer)}
                                            </td>
                                        </tr>
                                        <tr className="py-2 px-1">
                                            <td className="clr2 px-2 pl-4 py-2">
                                                {t('Publishers.1')}
                                            </td>
                                            <td className="clr3 px-2 py-2">
                                                {useCapitalize(data.publisher)}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            {/* Platforms(Right Side) */}
                            <div className="col-md-6 col-sm-12 platform pt-3 px-0">
                                <h4 className="mb-3 pt-1">
                                    <i className="fas fa-gamepad red-title pl-1"></i>
                                    <span className="fnt-light title"> {t('Platforms.1')}</span>
                                </h4>
                                <div className="items font-size-1">
                                    {mapPlatform}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
