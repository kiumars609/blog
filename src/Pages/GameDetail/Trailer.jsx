import React from 'react'
import { useTranslation } from 'react-i18next';
import './style.css'

export default function Trailer({ data }) {
    const { t, i18n } = useTranslation();
    return (
        <>
            <h1 className='mt-5 title-trailer'>{t('Trailer.1')}</h1>
            <div className="col-12 p-0 main-trailer">
                <embed type="video/webm" src={data.trailer} className='col-12 p-0 rounded shadow' allowFullScreen />
            </div>
        </>
    )
}