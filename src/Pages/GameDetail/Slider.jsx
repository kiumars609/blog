import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import './style.css'

export default function Slider({ data }) {
    const { t, i18n } = useTranslation();
    return (
        <>
            <h1 className='mt-5 title-slider'>{t('Images.1')}</h1>
            <div id="carouselExampleFade" className="carousel slide carousel-fade col-12 rounded shadow p-0 main-slider" data-bs-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-item active" data-bs-interval="4000">
                        {!data.imgSlider1 ?
                            <img src="https://gamefa.com/wp-content/uploads/2022/05/IMG_20220502_161058_218.jpg.webp" className="d-block w-100 rounded" alt="..." />
                            :
                            <img src={data.imgSlider1} className="d-block w-100 rounded" alt={data.title} />
                        }
                    </div>
                    {data.imgSlider2 &&
                        <div className="carousel-item" data-bs-interval="4000">
                            <img src={data.imgSlider2} className="d-block w-100 rounded" alt={data.title} />
                        </div>
                    }
                    {data.imgSlider3 &&
                        <div className="carousel-item" data-bs-interval="4000">
                            <img src={data.imgSlider3} className="d-block w-100 rounded" alt={data.title} />
                        </div>
                    }
                    {data.imgSlider4 &&
                        <div className="carousel-item" data-bs-interval="4000">
                            <img src={data.imgSlider4} className="d-block w-100 rounded" alt={data.title} />
                        </div>
                    }
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
        </>
    )
}
