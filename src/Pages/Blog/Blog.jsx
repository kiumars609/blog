import React, { useEffect, useState } from 'react'
import Layout from '../../Component/Layout'
import Loading from '../../Component/Loading/Loading';
import { get } from '../../services/HttpClient';
import Pagination from '../../hooks/usePagination';
import SinglePost from './SinglePost'

export default function Blog() {
    const [data, setData] = useState([]);
    const [loader, setLoader] = useState(true);
    // Start Pagination
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(5);
    // End Pagination
    useEffect(() => {
        get('/posts/postsAPI.php')
            .then(response => {
                setData(response);
                setLoader(false)
            })
    }, []);

    // Get Current Posts (Pagination)
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = data && data.slice(indexOfFirstPost, indexOfLastPost);


    // Change Page (Pagination)
    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    const mapData = currentPosts && currentPosts.map((row, index) => {
        return (
            <SinglePost
                key={index}
                id={row.id}
                title={row.title}
                text={row.text}
                datePost={row.createdAt}
                image={row.image}
                categoryTitle={row.categoryTitle}
                like={row.like}
            />
        )
    })

    return (
        <>
            <Layout hotNews={true}>
                {loader ? <Loading /> : mapData}
                <div className="section-container">
                    <div className="pagination-container">
                        <Pagination postsPerPage={postsPerPage} totalPosts={data && data.length} paginate={paginate} />
                    </div>
                </div>
            </Layout>
        </>
    )
}
