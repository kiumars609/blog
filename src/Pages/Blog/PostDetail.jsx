import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import HotNews from '../../Component/HotNews/HotNews';
import Layout from '../../Component/Layout'
import { get, post, put } from '../../services/HttpClient';
import { useTranslation } from 'react-i18next';
import './style.css'
import Tostify from '../../Component/Tostify'
import { toast } from 'react-toastify';
import { AvatarGenerator } from 'random-avatar-generator';

export default function PostDetail() {
    const { t, i18n } = useTranslation();
    const fetchId = useParams();
    const singleId = fetchId.id.slice(3);
    const [data, setData] = useState([]);
    const [avatar, setAvatar] = useState([]);
    const [date, setDate] = useState('');
    const [likeNumber, setLikeNumber] = useState(0);
    const [userComment, setUserComment] = useState('');
    const [textComment, setTextComment] = useState('');
    const [comments, setComments] = useState([]);
    const generator = new AvatarGenerator();
    // Fetch Post with special Id
    useEffect(() => {
        get(`/posts/postsAPI.php?${fetchId.id}`)
            .then(response => {
                for (let index = 0; index < response.length; index++) {
                    if(response[index].id === singleId){
                        const element = response[index];
                        setData(element);
                        setDate(data.createdAt);
                        setLikeNumber(element.like)
                    }
                }
            })

    }, [singleId,fetchId.id,data.createdAt]);
    // 

    // Fetch Admin Avatar
    useEffect(() => {
        get(`https://6207e35722c9e90017d32f73.mockapi.io/fav-icon`)
            .then(response => {
                setAvatar(response[0].icon)
            })
    }, []);
    // 

    // Handle Likes
    // const handleLike = (number) => {
    //     put(`https://6207e35722c9e90017d32f73.mockapi.io/blog/${singleId}`, { like: number + 1 })
    //         .then(
    //             setLikeNumber(number + 1)
    //         )
    // }


    const handleLike = (number) => {
        // put(`https://6207e35722c9e90017d32f73.mockapi.io/blog/${singleId}`, { like: number + 1 })
        //     .then(
        //         setLikeNumber(number + 1)
        //     )

        console.log(typeof singleId);

        const gg = parseInt(number)
        const rr = parseInt(singleId)

        // console.log((gg + 1));

        const likePayload = {
            id: rr,
            like: gg + 1
        }
        put(`/posts/postLikeAPI.php`,
            JSON.stringify(likePayload)).then(res => { })
            .then(
                setLikeNumber(gg + 1)
            )
    }

    // 

    // Submit Form Comment
    const handleSubmit = (e) => {
        e.preventDefault();
        if (userComment !== '' && textComment !== '') {
            const payload = {
                username: userComment,
                text: textComment,
                post_id: singleId,
                avatar: generator.generateRandomAvatar()
            }
            post('/comments/commentsInsertAPI.php',
            JSON.stringify(payload)).then(res=>{
                
            })
                .then(function (response) {
                    setUserComment('');
                    setTextComment('');
                    toast['success']('Your Comment Inserted Successfully !!')
                })
        }
        else {
            toast['error']('Please fill all of the fields.')
        }
    }
    // 

    // Fetch Comments
    useEffect(() => {
        get('/comments/commentsAPI.php')
            .then(response => {
                setComments(response)
            })
    }, [userComment]);

    const commentsList = comments.map((item, index) => {
        const fetchCreateAt = item.createdAt.slice(0, 10).replace(/-/g, '/');
        if (item.post_id === singleId) {
            return (
                <div className="col-12 main-comment" key={index}>
                    <div className="row">
                        <img src={item.avatar} className='col-1 avatar' alt='' />
                        <span className='col-5 pl-0 username'>{item.username}</span>
                        <span className='col-6 text-right date-released'>{fetchCreateAt}</span>
                    </div>
                    <p>
                        {item.text}
                    </p>
                </div>
            )
        }
    })
    // 

    return (
        <>
            <Layout>
                <Tostify />
                <div className="col-12 bg-white rounded shadow">
                    <h3 className='pt-3'>
                        {data.title}
                    </h3>
                    <hr />
                    <div className="row writer">
                        <div className="col-6"><img src={avatar} className='writer-avatar' alt='' /> <span className='writer-name'>Admin</span></div>
                        {/* <div className="col-6 text-right writer-release-post"><i className='fa fa-clock'></i> {fetchDate}</div> */}
                    </div>
                    <img src={data.image} className='col-12 rounded px-0 my-3' alt='' />
                    <p className='pb-4'>
                        {data.text}
                    </p>
                    <div className="col-12 d-flex justify-content-end pr-0 pb-3">
                        <div className="col-md-6 col-sm-12 row d-flex justify-content-end nowrap">
                            <span className='d-block col-2 px-0 like-btns' onClick={(e) => handleLike(likeNumber)}>{likeNumber} <i className='fa fa-thumbs-up'></i></span>
                        </div>
                    </div>
                </div>
                {/* Hot News */}
                <HotNews />
                {/* Comment */}
                <div className="col-12 bg-white rounded shadow my-3 pb-3">
                    <h5 className='fs-6 py-2 title-comments'>
                        {t('Comments.1')}
                    </h5>
                    {/* Form */}
                    <form onSubmit={(e) => handleSubmit(e)} className="mb-5">
                        <div className="col-md-6 px-0">
                            <input type='text' className='form-control' placeholder='Your Name' name='username' value={userComment} onChange={(e) => setUserComment(e.target.value)} />
                        </div>
                        <textarea className='form-control my-1' placeholder='Your Comment ...' name='text' onChange={(e) => setTextComment(e.target.value)} value={textComment}></textarea>
                        <button className='btn btn-primary mt-3'>Send Comment</button>
                    </form>
                    {/* Show Comments */}
                    {commentsList}
                </div>
            </Layout>
        </>
    )
}