import React from 'react'
import { Link } from 'react-router-dom'
import useMonths from '../../hooks/useMonths'
import { useTranslation } from 'react-i18next';
import './style.css'

export default function SinglePost({ like, title, text, datePost, image, id, categoryTitle }) {
    const { t, i18n } = useTranslation();
    const monthPost = datePost.split('-')
    const dayPost = monthPost[2].split(' ')

    return (
        <li>
            <div className="post-left-info">
                <div className="post-date">
                    <span className="day">{dayPost[0]}</span>
                    <span className="month">{useMonths(monthPost[1])}</span>
                </div>
                <div className="post-likes">
                    <i className="fa fa-heart text-primary"></i>
                    <span className="number"> {like}</span>
                </div>
            </div>
            <div className="post-content">
                <div className="post-image">
                    <Link to={`/game-deutch/detail/id=${id}`} className='post-image-link position-relative'>
                        <div className="post-category">{categoryTitle.toUpperCase()}</div>
                        <div className="post-image-cover" style={{ backgroundImage: `url(${image})` }}></div>
                    </Link>
                </div>
                <div className="post-info">
                    <h4 className="post-title">
                        <Link to={`/game-deutch/detail/id=${id}`}>{title}</Link>
                    </h4>
                    <div className="post-by">
                        {t('Posted-By.1')} <a href>Admin</a> <span className="divider">|</span> {categoryTitle.toUpperCase()} <span className="divider">|</span> 
                        {/* 12 {t('Comments.1')} */}
                    </div>
                    <div className="post-desc">
                        {text}
                    </div>
                </div>
                <div className="read-btn-container">
                    <Link to={`/game-deutch/detail/id=${id}`} className="read-btn">{t('Read-More.1')} <i className="fa fa-angle-double-right"></i></Link>
                </div>
            </div>
        </li>
    )
}
